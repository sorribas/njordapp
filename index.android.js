/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image
} from 'react-native';
const PushNotification = require('react-native-push-notification');

export default class aeolus extends Component {
  constructor() {
    super();
    this.state = {};

    let id = 1;
    const update = () => {
      const percentPromise = fetch('https://njordapp.herokuapp.com/')
        .then(res => res.json())
        .then(res => res.renewable / (res.renewable + res.fossil))
        .then(fraction => (100 * fraction).toFixed(2));
      percentPromise.then(percent => {
        console.log('updating state', percent);
        this.setState({percent})
        // this.state = {percent}
      });
      percentPromise.then(function (percent) {
        PushNotification.cancelAllLocalNotifications();
        var percentile = Math.round(percent / 10);
        PushNotification.localNotification({
          title: "Njord App",
          message: `${percent}% clean energy`,
          vibrate: false,
          playSound: false,
          smallIcon: `battery_${percentile}`,
          id: ++id
        });
      });
    };

    update();
    setInterval(update, 60000 * 5);
  }

  render() {
    console.log('rendering', this.state);
    if (this.state.percent) return (
      <View style={styles.container}>
        {images[Math.round(this.state.percent / 10)]} 
        <Text style={styles.welcome}>
          Generating {this.state.percent} % of renewable energy.
        </Text>
      </View>
    );
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Updating data...
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

const images = [
  (<Image
    source={require(`./android/app/src/main/res/mipmap-xxhdpi/battery_0.png`)}
  />),
  (<Image
    source={require(`./android/app/src/main/res/mipmap-xxhdpi/battery_1.png`)}
  />),
  (<Image
    source={require(`./android/app/src/main/res/mipmap-xxhdpi/battery_2.png`)}
  />),
  (<Image
    source={require(`./android/app/src/main/res/mipmap-xxhdpi/battery_3.png`)}
  />),
  (<Image
    source={require(`./android/app/src/main/res/mipmap-xxhdpi/battery_4.png`)}
  />),
  (<Image
    source={require(`./android/app/src/main/res/mipmap-xxhdpi/battery_5.png`)}
  />),
  (<Image
    source={require(`./android/app/src/main/res/mipmap-xxhdpi/battery_6.png`)}
  />),
  (<Image
    source={require(`./android/app/src/main/res/mipmap-xxhdpi/battery_7.png`)}
  />),
  (<Image
    source={require(`./android/app/src/main/res/mipmap-xxhdpi/battery_8.png`)}
  />),
  (<Image
    source={require(`./android/app/src/main/res/mipmap-xxhdpi/battery_9.png`)}
  />),
  (<Image
    source={require(`./android/app/src/main/res/mipmap-xxhdpi/battery_10.png`)}
  />)
]

AppRegistry.registerComponent('aeolus', () => aeolus);
